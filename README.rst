===========
Summon
===========

Invite ("summon") potential users by email.

Specific features

* Admin interface and management commands to send invites.
* Management commands to resend, purge, and expire invites.
* Much customisation through settings and templates.


Quick Start
-----------

1. Install via the usual methods, e.g pip install django-summon

2. Add "summon" to your INSTALLED_APPS::

   INSTALLED_APPS = (
      ...

      'summon',

      ...
   )

3. Add the following to your settings::

   SUMMON_SEND_ON_SAVE = True

4. Add the following to your top-level urls.py patterns::

   url(r'^invite/', include('summon.urls', namespace='summon')),
   url(r'^account/signup/', SomeRegistrationView.as_view(), name='account_signup')

5. Run `python manage.py migrate summon` to create the relevant models.

6. From the admin console, add Summonings. Invites will be automatically sent
   upon being saved (this is due to step 3 above).


Template Variables
------------------

These are the variables available to templates, assuming everything else is
standard:

owner
  The owner of the invitation (may be None).

email
  The invitation recipient's email.

full_name
  The invitation recipient's full name (may be None).

short_name
  The invitation recipient's short name (may be None).

site
  The django site object originating the invitation.

site_name
  The site name, either configured (see below) or from the site object.

invite_url
  The URL for acepting the invitation.

invite_expiry
  The python datetime representing when the invitation will expire.

invite:
  The invitation object itself.


Configuration
-------------

These are likely the most common-used settings
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

SUMMON_SEND_ON_SAVE
  If True, invites are sent the first time they are saved.  Otherwise you will
  need to use the resend task, or arrange them to be sent via your own code.
  Default = False.

SUMMON_SITE_NAME
  Customize the name used in emails (e.g. your invitation
  to join Fabulous Amazing Site!'). Default = None, in which case the current
  site name will be used::

      SUMMON_SITE_NAME = 'Really Quite Fantastic Site'

SUMMON_FORWARD_ARGS
  If True, pass the invite key as a kwarg to the URL to which a user is
  redirected upon accepting a valid invite.

SUMMON_EXPIRE_INVITES
  If True, invites will be given an expiration date, beyond which the invitation
  will be considered invalid. If False, invites never expire. Default = False

SUMMON_KEY_EXPIRY_MS
  If SUMMON_EXPIRE_INVITES is True, this is the number of milliseconds after an
  invite is created before it expires. Default is 3 days::

      SUMMON_KEY_EXPIRES = 30 * 86400 * 1000   # expire after 30 days

SUMMON_REPLY_TO
  Set the reply-to address used in summonings. Useful where you don't want the
  recipient to reply to an invitation::

      SUMMON_REPLY_TO = 'Do Not Reply <no-reply@domain.com>'


Other common settings
~~~~~~~~~~~~~~~~~~~~~

SUMMON_ACCEPT_ON_LOAD
  If True (the default), an invitation will be marked as accepted when its
  URL is loaded in some way, typically via clicking on the link in an
  invitation email. If this is set to False, an invitation will not be marked
  as accepted until the user registers an account - this is something you will
  have to perform in your registration code, since summon does not handle
  registration. In this case the URL will remain valid until the invitation
  expires, even if the user has loaded it.

SUMMON_SITE_HOST
  Customize the host (domain) for generated invite URLs.  Default = None, in which
  case the current site will be used::

      SUMMON_SITE_HOST = 'www.my.domain.com'

SUMMON_SITE_SCHEME
  Customize the scheme for generated invite URLs. Default = 'http'::

      SUMMON_SITE_SCHEME = 'https'

SUMMON_LOGIN_URL
  A URL name or path to which valid users will be redirected if a valid invite has
  already been accepted. Default = None, which forces use of the standard LOGIN_URL
  setting (falling back to a URL named 'account_login')::

      SUMMON_LOGIN_URL = 'members:login'    # Named URL
      # or
      SUMMON_LOGIN_URL = '/members/signin'  # URL path

SUMMON_REGISTER_URL
  A URL name or path to which users will be redirected upon successful validation of
  an invite URL. Default = None, falling back to the URL named 'account_signup'::

      SUMMON_REGISTER_URL = 'members:register'   # Named URL. Must accept a
      parameter 'key' to receive the invitation key, if SUMMON_FORWARD_ARGS is True
      # or
      SUMMON_REGISTER_URL = '/members/begin'     # URL path, useful for SPA-style apps.

  If you provide this as a URL path, and you also set SUMMON_FORWARD_ARGS to
  True, the invitation token will be appended to the URL with a leading / if
  needed (and a trailing slash if SUMMON_APPEND_SLASH is True)::

      SUMMON_REGISTER_URL = '/members/signup'
      SUMMON_FORWARD_ARGS = True

      Respondents will be redirected to /members/signup/<key>

  You can override this to use the query string via SUMMON_FORWARD_VIA_QUERY_STRING.

SUMMON_FORWARD_VIA_QUERY_STRING
  If True, the forwarded invitation key will be passed via the query string
  (if relevant)::

      SUMMON_REGISTER_URL = '/members/signup'
      SUMMON_FORWARD_ARGS = True
      SUMMON_FORWARD_VIA_QUERY_STRING = True

      Respondents will be redirected to /members/signup?key=<key>

SUMMON_APPEND_SLASH
  If True, path-based URLs that receive an invitation key in the path will
  also have a trailing slash appended. Defaults to APPEND_SLASH.


Other not-so-common settings
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You should never need to set these explicitly. It is recommended you do not change
these unless you know exactly what you are doing.

SUMMON_ACCEPT_URL
  A named URL name used to generate invite acceptance URLs. This URL **must** accept
  a path segment named 'key'. Default = 'summon:accept'::

      SUMMON_ACCEPT_URL = 'invites:accept'

SUMMON_KEY_GENERATOR
  An importable class name that exposes a generate_key(length) method::

      SUMMON_KEY_GENERATOR = 'my.app.utils.KeyGenerator'

SUMMON_MAIL_PROCESSOR
  An importable class name that exposes a send(summoning, *args, **kwargs) method.
  This method must perform ALL necessary functionality of rendering message templates,
  sending the email etc::

      SUMMON_MAIL_PROCESSOR = 'my.app.utils.MailProcessor'

  It is typical to subclass the provided summon.backend.MailProcessor class,
  which does all of the heavy lifting.


Templates
---------

The following templates can be overridden::

    summon/
           email/
                 body.txt
                 subject.txt

           error/
                 expired_key.html
                 invalid_key.html


If you also provide::

    body.html

in the same place as its text variant, it will be used during email
rendering.


Sending
-------

It is possible to pass in various parameters at send-time. These are passed as
kwargs to the send() method, and are passed verbatim to the mail processor's
send() method. Currently understood parameters::

sender
  The value to use as the email's "from" value. If not provided, this will be
  retrieved from either settings.SUMMON_FROM_EMAIL or
  settings.DEFAULT_FROM_EMAIL.

subject_template
  The template to use for rendering the email subject line.

body_template
  The template to use for rendering the email message body.


Management Commands
-------------------

summon
   Invite users by email, optionally providing an owner::

        # Invite a single user
        ./manage.py summon --to=a_user@someserver.com
        # Invite multiple users
        ./manage.py summon --to=anotherguy@mydomain.org --to=thatdude@hisdomain.org
        # Can also provide an owner (an existing user to manage the invitation)
        ./manage.py summon --to=somelady@herdomain.net --owner=bob
        # Full/ short names are now supported
        ./manage.py summon --to=somelady@herdomain.net --full-name 'Some Lady'
        # Set an invite to expire at some time
        ./manage.py summon --to=somedude@ourdomain.net --expire '2018/01/01 16:00:00 UTC'


resend
    Resend (or send) invites by some criteria::
        # A specific invitee by email
        ./manage.py resend --to=a_user@someserver.com
        # Can mix'n'match with invite keys
        ./manage.py resend --to=can_also_be_an_invite_key --to=somelady@herdomain.net
        # Resend all UNACCEPTED invites.
        ./manage.py resend --all

    Resent invites re-build the sent email from scratch, rather than
    re-sending a canned message. This allows you to change the message
    template prior to re-sending if desired.


expiresummons
    Expire invites by some criteria, preventing their acceptance::
        # A specific invitee by email
        ./manage.py expiresummons --to=a_user@someserver.com
        # Can mix'n'match with invite keys
        ./manage.py expiresummons --to=can_also_be_an_invite_key --to=somelady@herdomain.net
        # Expire all UNACCEPTED invites.
        ./manage.py expiresummons --all


purgesummons
    Remove invites by some criteria::
        # A specific invitee by email
        ./manage.py purgesummons --to=a_user@someserver.com
        # Can mix'n'match with invite keys
        ./manage.py purgesummons --to=can_also_be_an_invite_key --to=somelady@herdomain.net
        # Purge only accepted invites:
        ./manage.py purgesummons --accepted
        # Purge all unACCEPTED and all EXPIRED invites.
        ./manage.py purgesummons --all
        # Purge all by a certain owner
        ./manage.py purgesommons --owner=bob
        # Purge only expired, belonging to a certain owner
        ./manage.py purgesommons --owner=bob --expired


generateinviteuri
    Generate the URI for an existing invitation.


Motivations
-----------

This app grew out of my frustration with existing invitation packages, most of
which were languishing in Django 1.6 hell and had not been updated within the
past few years (as of the time of this document). The packages I looked at
were very simple and suitable only to serve as example code rather than being
full-featured apps. Mainly I needed a way to manage invitations from the
command line, override various settings and templates, and other tasks which
would have taken just as long to implement in a fork of one of the examined
packages, as to write from scratch.

Specifically, I needed the ability to pass an invite ID or key to my
registration page to filter out users who arrived there by some method other
than invitation. I also came to need the ability to provide a URL to a site
not served directly via django - an SPA which handled the registration process
via API calls, rather than via plain django views.

I also had working registration/ login processes that I didn't want to
duplicate; adding those to summon may seem like a 'batteries included'
approach, however I didn't want to duplicate effort, and separation of
concerns won out - do one thing, and do it well.

I won't go into the details of any other similar package here, since it
would just end up duplicating the djangopackages.org grid.


Coming Soon
-----------

* Summon users in bulk, via command line and API.
* Check for existing users (via standard/ custom User model, profiles etc).
* Add an 'expire at' parameter to expiresummons.
* Add a 'pending' feature, allowing users to self-register for a future
  invite; an admin can then go in and send the invites (at which point the
  users will receive an email as normal), or purge them.
* Provide DRF-compatible API to allow retrieval of token validity, etc.
* Use newer style settings: SUMMON = {...} vs SUMMON_FOO style.


Coming REAL Soon
----------------

* Generate signals on various events.

