from django.conf import settings
from django.db import models
from django.utils import timezone
from django.utils.module_loading import import_string

from . import backend


KEY_GENERATOR_CLASS = getattr(settings, 'SUMMON_KEY_GENERATOR',
                              'summon.backend.KeyGenerator')

KEY_GENERATOR = None

KEY_EXPIRY_MS = getattr(settings, 'SUMMON_KEY_EXPIRY_MS', 3 * 86400 * 1000)


MAIL_SENDER_CLASS = getattr(settings, 'SUMMON_MAIL_PROCESSOR',
                            'summon.backend.MailProcessor')

MAIL_SENDER = None


def get_generator():
    global KEY_GENERATOR
    if not KEY_GENERATOR:
        keygen_class = import_string(KEY_GENERATOR_CLASS)
        KEY_GENERATOR = keygen_class()

    return KEY_GENERATOR


def get_mail_sender():
    global MAIL_SENDER
    if not MAIL_SENDER:
        sender_class = import_string(MAIL_SENDER_CLASS)
        MAIL_SENDER = sender_class()

    return MAIL_SENDER


class Summoning(models.Model):

    class Meta(object):
        app_label = 'summon'

    owner = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True)
    email = models.EmailField(max_length=254, unique=True)
    key = models.CharField(max_length=64, unique=True)
    created_date = models.DateTimeField(auto_now_add=True)
    accepted_date = models.DateTimeField(blank=True, null=True)
    expiry_date = models.DateTimeField(blank=True, null=True)
    sent_date = models.DateTimeField(blank=True, null=True)
    full_name = models.CharField(max_length=256, null=True, blank=True)
    short_name = models.CharField(max_length=64, null=True, blank=True)

    def __str__(self):
        return u'%s:%s' % (self.email, self.key)

    def accept(self, timestamp=None):
        # TODO(simon) (Aug 12, 2016): emit signal
        self.accepted_date = timestamp or timezone.now()
        self.save()

    # pylint: disable=no-self-use
    def calculate_expiry(self):
        return timezone.now() + timezone.timedelta(milliseconds=KEY_EXPIRY_MS)

    def expire(self, timestamp=None):
        self.expiry_date = timestamp or timezone.now()
        self.save()

    def get_absolute_url(self):
        return backend.get_absolute_uri(self)

    @property
    def is_accepted(self):
        return self.accepted_date is not None

    @property
    def is_expired(self):
        return self.expiry_date and self.expiry_date < timezone.now()

    def save(self, *args, **kwargs):
        send_on_save = False
        if not self.id:
            send_on_save = getattr(settings, 'SUMMON_SEND_ON_SAVE', False)

            if not self.key:
                self.key = get_generator().generate_key(length=64)

        expire_invites = getattr(settings, 'SUMMON_EXPIRE_INVITES', False)
        if expire_invites and not self.expiry_date:
            self.expiry_date = self.calculate_expiry()

        super(Summoning, self).save(*args, **kwargs)
        if send_on_save:
            self.send()

    def send(self, **kwargs):
        try:
            get_mail_sender().send(self, **kwargs)

        except Exception as e:
            raise e

        else:
            # TODO(simon) (Aug 12, 2016): emit signal
            self.sent_date = timezone.now()
            self.save()
