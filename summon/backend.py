import os

from django.conf import settings
from django.contrib.sites.models import Site
from django.core import mail
from django.shortcuts import resolve_url
from django.template import TemplateDoesNotExist
from django.template.loader import render_to_string
from django.utils import crypto


def get_absolute_uri(summoning, request=None, site=None):
    '''Return the absolute URI for a Summoning instance.

    This is useful for including in emails etc.

    Args:
        request: A django request object.
        site: A django.contrib.sites.Site object.

    If request is provided, it will only be used if no other
    acceptable method for generating the URI can be found. This
    is because passing in a request instance may be a side-effect
    of other code, whereas providing non-default settings is a
    deliberate statement that those settings should be used.

    In other words, explicit site overrides custom settings.SUMMON_SITE_HOST
    which overrides request.
    '''
    url = resolve_url(getattr(settings, 'SUMMON_ACCEPT_URL', 'summon:accept'),
                      key=summoning.key)

    if hasattr(settings, 'SUMMON_SITE_HOST'):
        scheme = getattr(settings, 'SUMMON_SITE_SCHEME', 'http')
        host = settings.SUMMON_SITE_HOST

        # TODO(simon) (Jun 18, 2018): use urljoin
        return ''.join([scheme, '://', host, url])

    if hasattr(settings, 'SITE_URL'):
        # TODO(simon) (Jun 18, 2018): use urljoin?
        return ''.join([settings.SITE_URL, url])

    if site:
        request = None

    else:
        site = Site.objects.get_current()

    if request:
        return request.build_absolute_uri(url)

    scheme = getattr(settings, 'SUMMON_SITE_SCHEME', 'http')
    host = site.domain

    # TODO(simon) (Jun 18, 2018): use urljoin
    return ''.join([scheme, '://', host, url])


class KeyGenerator(object):

    # pylint: disable=no-self-use
    def generate_key(self, length=64):
        return crypto.get_random_string(length)


class MailProcessor(object):

    email_body_template = 'summon/email/body.txt'
    email_subject_template = 'summon/email/subject.txt'

    # pylint: disable=no-self-use
    def _create_message(self, subject, sender, to, bodies, **kwargs):
        msg_args = {}
        reply_to = None

        if kwargs.get('reply_to', None) is not None:
            reply_to = kwargs['reply_to']

        elif hasattr(settings, 'SUMMON_REPLY_TO'):
            reply_to = settings.SUMMON_REPLY_TO

        if reply_to:
            if not isinstance(reply_to, (tuple, list)):
                reply_to = [reply_to]
            msg_args['reply_to'] = reply_to

        if 'txt' in bodies:
            msg = mail.EmailMultiAlternatives(subject,
                                              bodies['txt'],
                                              sender,
                                              [to],
                                              **msg_args)

            if 'html' in bodies:
                msg.attach_alternative(bodies['html'], 'text/html')

        else:
            msg = mail.EmailMessage(subject, bodies['html'], sender, [to],
                                    **msg_args)
            msg.content_subtype = 'html'

        return msg

    def _render_body(self, tpl, context):
        return render_to_string(tpl, context).strip()

    def _render_mail(self, sender, to, context, **kwargs):
        base_template = kwargs.get('body_template', self.email_body_template)
        subj_template = kwargs.get('subject_template',
                                   self.email_subject_template)

        subject = self._render_subject(context, subj_template)

        bodies = {}
        template_prefix, _ = os.path.splitext(base_template)
        for ext in ['html', 'txt']:
            try:
                template_name = '.'.join([template_prefix, ext])
                bodies[ext] = self._render_body(template_name, context)

            except TemplateDoesNotExist:
                if ext == 'txt' and not bodies:
                    # We need at least one body
                    raise

        return self._create_message(subject, sender, to, bodies, **kwargs)

    # pylint: disable=no-self-use
    def _render_subject(self, context, template):
        subject = render_to_string(template, context)
        subject = ' '.join(subject.splitlines()).strip()
        return subject

    def _send(self, owner, sender, summoning, **kwargs):
        site = kwargs.get('site', Site.objects.get_current())

        context = self.get_mail_context(site, owner, summoning)

        to = summoning.email
        msg = self._render_mail(sender, to, context, **kwargs)
        msg.send(fail_silently=kwargs.get('fail_silently', True))

    # pylint: disable=no-self-use
    def get_mail_context(self, site, owner, summoning):
        invite_url = get_absolute_uri(summoning)
        site_name = getattr(settings, 'SUMMON_SITE_NAME', site.name)

        context = {'owner': owner,
                   'email': summoning.email,
                   'full_name': summoning.full_name,
                   'short_name': summoning.short_name,
                   'site': site,
                   'site_name': site_name,
                   'invite_url': invite_url,
                   'invite_expiry': summoning.expiry_date,
                   'invite': summoning}

        return context

    def send(self, summoning, **kwargs):
        owner = summoning.owner
        sender = kwargs.pop('sender', None)
        if sender is None:
            sender = getattr(settings, 'SUMMON_FROM_EMAIL',
                             settings.DEFAULT_FROM_EMAIL)
        self._send(owner, sender, summoning, **kwargs)
