import logging

from django import http
from django.conf import settings
from django.core import urlresolvers as urls
from django.shortcuts import redirect
from django.shortcuts import resolve_url
from django.template.loader import render_to_string
from django.views.generic import View
from django.views.generic.detail import SingleObjectMixin


from . import models


LOGGER = logging.getLogger(__name__)


def _get_forwarded_arg_url(url_name, summoning):
    try:
        url = urls.reverse(url_name, kwargs={'key': summoning.key})

    except (urls.Resolver404, urls.NoReverseMatch):
        if getattr(settings, 'SUMMON_FORWARD_ARGS_VIA_QUERY_STRING', False):
            url = url_name
            if '?' in url:
                url = '{0}&key={1}'.format(url_name, summoning.key)

            else:
                url = '{0}?key={1}'.format(url_name, summoning.key)

        else:
            url = url_name
            if url.endswith('/'):
                url += summoning.key

            else:
                url += '/' + summoning.key

            if getattr(settings, 'SUMMON_APPEND_SLASH', settings.APPEND_SLASH):
                url += '/'

    else:
        url = resolve_url(url_name, key=summoning.key)

    return url


class AcceptView(SingleObjectMixin, View):

    expired_key_template = 'summon/error/expired_key.html'
    invalid_key_template = 'summon/error/invalid_key.html'

    # py2.7 wants key=None before *args; py3.7 (and lint) wants it after
    # pylint: disable=keyword-arg-before-vararg
    def get(self, request, key=None, *args, **kwargs):
        return self.process(request, key, *args, **kwargs)

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()

        try:
            return queryset.get(key=self.kwargs['key'])

        except models.Summoning.DoesNotExist:
            raise http.Http404()

    def get_queryset(self):
        return models.Summoning.objects.all()

    def process(self, request, key, *args, **kwargs):
        try:
            summoning = self.get_object()

        except http.Http404:
            # TODO(simon) (Aug 13, 2016): maybe allow a flag to determine what
            # to do with the user.
            return self.response_invalid(request)

        if summoning.is_expired:
            # TODO(simon) (Aug 13, 2016): maybe allow a flag to determine what
            # to do with the user.
            return self.response_expired(request, summoning)

        return self.response_valid(request, summoning)

    # pylint: disable=no-self-use
    def response_accepted(self, request, summoning, *args, **kwargs):
        LOGGER.warning('Summoning {} is already accepted'.format(summoning))
        url = getattr(settings, 'SUMMON_LOGIN_URL',
                      getattr(settings, 'LOGIN_URL',
                              'account_login'))

        return redirect(resolve_url(url))

    def response_expired(self, request, summoning, *args, **kwargs):
        LOGGER.warning('Summoning {} is expired'.format(summoning))
        tpl = getattr(settings, 'SUMMON_EXPIRED_KEY_TEMPLATE',
                      self.expired_key_template)
        return http.HttpResponseGone(render_to_string(tpl))

    def response_invalid(self, request, *args, **kwargs):
        LOGGER.warning('Requested summoning is invalid')
        tpl = getattr(settings, 'SUMMON_INVALID_KEY_TEMPLATE',
                      self.invalid_key_template)
        return http.HttpResponseNotFound(render_to_string(tpl))

    def response_valid(self, request, summoning, *args, **kwargs):
        LOGGER.warning('Summoning {} is valid'.format(summoning))
        if summoning.is_accepted:
            return self.response_accepted(request, summoning, *args, **kwargs)

        if getattr(settings, 'SUMMON_ACCEPT_ON_LOAD', True):
            summoning.accept()

        url_name = getattr(settings, 'SUMMON_REGISTER_URL',
                           'account_signup')

        if getattr(settings, 'SUMMON_FORWARD_ARGS', False):
            url = _get_forwarded_arg_url(url_name, summoning)

        else:
            url = resolve_url(url_name)

        LOGGER.warning('Redirecting url ({}) for ({}) to {}'.format(
                url,
                url_name,
                redirect(url)))
        return redirect(url)
