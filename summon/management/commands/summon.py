import dateparser
import pytz

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.core.management.base import CommandError
from django.db.models import Q
from django.db import transaction
from django.db.utils import IntegrityError
from django.utils import timezone

from ... import models as summon


class Command(BaseCommand):

    help = 'Summon users by email.'

    def add_arguments(self, parser):
        super(Command, self).add_arguments(parser)

        parser.add_argument('--create-only',
                            action='store_true',
                            dest='create_only',
                            default=False,
                            help=('Create summonings, but do not send. If'
                                  ' SEND_ON_SAVE=True, summonings WILL'
                                  ' be sent.'))

        parser.add_argument('--expire',
                            action='store',
                            dest='expiry',
                            default=None,
                            help=('Expiration timestamp in any dateparser'
                                  ' accepted form. Unless specified, timezone'
                                  ' will be UTC.'))

        parser.add_argument('--fail-silently',
                            action='store_false',
                            dest='fail_silently',
                            default=True,
                            help=('If false, invites that fail to send will'
                                  ' be displayed.'))

        parser.add_argument('--full-name',
                            action='store',
                            dest='full_name',
                            default=None,
                            help=('Full name of recipient. Only valid'
                                  ' if a single recipient is provided.'))

        parser.add_argument('--input-file',
                            action='store',
                            dest='input_file',
                            default=None,
                            help=('CSV file containing invite data.'
                                  'Each row as email,full_name,short_name'))

        parser.add_argument('--has-header',
                            action='store_true',
                            dest='has_header',
                            default=False,
                            help='Input file has a single header row.')

        parser.add_argument('--owner',
                            action='store',
                            dest='owner',
                            default=None,
                            help=('Username or PK of owner. If not provided,'
                                  ' no owner will be set.'))

        parser.add_argument('--reply-to',
                            action='append',
                            dest='reply_to',
                            default=[],
                            help=('Reply-to if desired.'))

        parser.add_argument('--sender',
                            action='store',
                            dest='sender',
                            default=None,
                            help=('Sender in form user@server.com or'
                                  '"User Name <user@server.com>"'))

        parser.add_argument('--short-name',
                            action='store',
                            dest='short_name',
                            default=None,
                            help=('Short name of recipient (e.g. first name).'
                                  ' Valid as for full-name.'))

        parser.add_argument('--to',
                            action='append',
                            dest='to',
                            default=[],
                            help='Email to invite. Multiples allowed.')

    def handle(self, *unused_args, **options):
        verbosity = int(options['verbosity'])

        if not options['to']:
            raise CommandError('Please provide at least one recipient email.')

        # pylint: disable=line-too-long
        if len(options['to']) > 1 and (options['full_name'] or options['short_name']):
            raise CommandError('Do not provide either full or short name with'
                               ' multiple recipients.')

        if options['owner']:
            owner_id = str(options['owner'])
            if owner_id.isdigit():
                query = Q(pk=owner_id) | Q(username=owner_id)

            else:
                query = Q(username=owner_id)

            owner = get_user_model().objects.get(query)

        else:
            owner = None

        for to in options['to']:
            obj_kwargs = {'email': to,
                          'owner': owner,
                          'full_name': options['full_name'],
                          'short_name': options['short_name']}

            if options['expiry']:
                dp_settings = {'TO_TIMEZONE': 'UTC', 'TIMEZONE': 'UTC'}
                expiry_date = dateparser.parse(options['expiry'],
                                               settings=dp_settings)
                if not expiry_date.tzinfo:
                    expiry_date = timezone.make_aware(expiry_date)

                obj_kwargs['expiry_date'] = expiry_date.astimezone(pytz.utc)

            summoning = summon.Summoning(**obj_kwargs)

            try:
                with transaction.atomic():
                    summoning.save()

                if not (options['create_only'] or summoning.sent_date):
                    with transaction.atomic():
                        summoning.send(sender=options['sender'],
                                       reply_to=options['reply_to'])

            except IntegrityError as e:
                if verbosity > 0:
                    msg = ('Cannot summon "%s" - email will'
                           ' not be sent (%s).' % (to, e))
                    self.stderr.write(msg)
