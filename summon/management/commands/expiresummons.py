from ._base import SummonBaseCommand


class Command(SummonBaseCommand):
    '''Command to expire summonings.'''

    command = 'expire'
    help = 'Expire unaccepted summonings.'

    def add_arguments(self, parser):
        super(Command, self).add_arguments(parser)

        parser.add_argument('--to',
                            action='append',
                            dest='to',
                            default=[],
                            help='Email or key to expire. Multiples allowed.')

        parser.add_argument('--all',
                            action='store_true',
                            dest='all',
                            default=False,
                            help=('Expire ALL unaccepted invites.'
                                  ' Use with care.'))

    # pylint: disable=no-self-use
    def _execute(self, summonings):
        for summoning in summonings:
            summoning.expire()
