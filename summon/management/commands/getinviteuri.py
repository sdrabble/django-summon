from django.core.management.base import BaseCommand
from django.core.management.base import CommandError

from ... import backend
from ... import models as summon


class Command(BaseCommand):

    help = 'Generate a URI for a summoning.'

    def add_arguments(self, parser):
        super(Command, self).add_arguments(parser)

        parser.add_argument('-k', '--key',
                            action='store',
                            dest='key',
                            default=None,
                            help='Summoning key')

    def handle(self, *unused_args, **options):
        if not options['key']:
            raise CommandError(
                    'Please provide a summoning key, ID, or email address.')

        try:
            summoning = summon.Summoning.objects.get(key=options['key'])

        except summon.Summoning.DoesNotExist:
            if str.isdigit(str(options['key'])):
                summoning = summon.Summoning.objects.get(pk=options['key'])

            else:
                summoning = summon.Summoning.objects.get(email=options['key'])

        self.stdout.write(backend.get_absolute_uri(summoning))
