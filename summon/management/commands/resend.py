from ._base import SummonBaseCommand


class Command(SummonBaseCommand):
    '''Command to resend summonings.'''

    command = 're-summon'
    help = ('Re-sends email to UNACCEPTED invites.'
            ' Can also send any un-sent invites.')

    def add_arguments(self, parser):
        super(Command, self).add_arguments(parser)

        parser.add_argument('--to',
                            action='append',
                            dest='to',
                            default=[],
                            help='Email or key to resend. Multiples allowed.')

        parser.add_argument('--all',
                            action='store_true',
                            dest='all',
                            default=False,
                            help=('Send ALL unaccepted invites. Useful if e.g.'
                                  ' your acceptance URL has changed.'
                                  ' Use with care.'))

    # pylint: disable=no-self-use
    def _execute(self, summonings):
        for summoning in summonings:
            summoning.send()
