from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.core.management.base import CommandError
from django.db.models import Q

from ... import models


class Command(BaseCommand):

    help = ('Remove targeted, accepted, and/ or expired summonings.'
            '\nAny specific or owner-based summonings are removed regardless'
            ' of status.'
            '\nThis may result in invitations that cannot be accepted.'
            '\nIf an owner is specified along with either expired or accepted'
            ' (or both),\nonly the matching invitations are affected.\nE.g. if'
            " passed --owner=bob --expired,\nthen only Bob's expired"
            ' invitations will be removed.')

    def add_arguments(self, parser):
        super(Command, self).add_arguments(parser)

        parser.add_argument('--to',
                            action='append',
                            dest='to',
                            default=[],
                            help='Email or key to remove. Multiples allowed.')

        parser.add_argument('--expired',
                            action='store_true',
                            dest='expired',
                            default=False,
                            help='Remove expired summonings.')

        parser.add_argument('--accepted',
                            action='store_true',
                            dest='accepted',
                            default=False,
                            help='Remove accepted summonings.')

        parser.add_argument('--owner',
                            action='store',
                            dest='owner',
                            default=None,
                            help='Username or PK of owner.')

        parser.add_argument('--all',
                            action='store_true',
                            dest='all',
                            default=False,
                            help=('Expire all accepted AND all expired invites.'
                                  ' Use with care.'))

    # pylint: disable=no-self-use
    def handle(self, *unused_args, **options):
        is_valid = (options['to'] or options['all'] or options['owner']
                    or options['expired'] or options['accepted'])

        User = get_user_model()  # pylint: disable=invalid-name

        if not is_valid:
            raise CommandError('Please provide at least one email or key,'
                               ' an owner, or use one of the --all,'
                               ' --expired, or --accepted options.')

        if options['owner']:
            owner_id = str(options['owner'])
            if owner_id.isdigit():
                query = Q(pk=owner_id) | Q(username=owner_id)

            else:
                query = Q(username=owner_id)

            owner = User.objects.get(query)

        else:
            owner = None

        if options['all']:
            query = Q(accepted_date=None) & Q(expiry_date=None)
            summonings = models.Summoning.objects.exclude(query)

        else:
            query = Q()
            if options['expired']:
                query |= ~Q(expiry_date=None)

            if options['accepted']:
                query |= ~Q(accepted_date=None)

            if owner:
                query &= Q(owner=owner)

            query |= Q(email__in=options['to']) | Q(key__in=options['to'])

            summonings = models.Summoning.objects.filter(query)

        for summoning in summonings:
            summoning.delete()
