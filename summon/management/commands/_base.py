from django.core.management.base import BaseCommand
from django.core.management.base import CommandError
from django.db.models import Q

from ... import models


class SummonBaseCommand(BaseCommand):

    command = ''

    def _execute(self, *args, **kwargs):
        raise NotImplementedError()

    def _handle_accepted_summonings(self, query, command=None):
        accepted_summonings = models.Summoning.objects.filter(
                query).exclude(accepted_date=None)

        for summon in accepted_summonings:
            msg = 'Cannot %s "%s" - already accepted.' % (
                    command or self.command or 'process',
                    summon.email)

            self.stderr.write(msg)

        return accepted_summonings

    def _handle_invalid_summonings(self, to, summonings, accepted_summonings):
        valid = {x.email for x in summonings}
        valid.update([x.key for x in summonings])

        invalid = set(to).difference(valid)
        invalid.difference_update([x.key for x in accepted_summonings])
        invalid.difference_update([x.email for x in accepted_summonings])

        for item in invalid:
            msg = 'Invalid value: "%s"' % (item, )
            self.stderr.write(msg)

    def _process(self, options):
        verbosity = int(options['verbosity'])

        query = Q(email__in=options['to']) | Q(key__in=options['to'])
        if verbosity > 0:
            accepted_summonings = self._handle_accepted_summonings(query)

        query &= Q(accepted_date=None)
        summonings = models.Summoning.objects.filter(query)

        if verbosity > 0:
            self._handle_invalid_summonings(options['to'], summonings,
                                            accepted_summonings)

        return summonings

    # pylint: disable=no-self-use
    def _verify_options(self, options):
        if not (options['to'] or options['all']):
            raise CommandError('Please provide at least one email or key,'
                               ' or use the --all option.')

    def handle(self, *args, **options):
        self._verify_options(options)

        if options['all']:
            summonings = models.Summoning.objects.filter(accepted_date=None)

        else:
            summonings = self._process(options)

        self._execute(summonings)
