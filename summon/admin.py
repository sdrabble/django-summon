from django.contrib import admin

from . import models


class SummoningAdmin(admin.ModelAdmin):

    list_display = ('email', 'full_name', 'sent_date', 'accepted_date',
                    'expiry_date')
    list_filter = ('accepted_date', 'expiry_date', 'sent_date')
    readonly_fields = ('key', 'accepted_date', 'expiry_date', 'sent_date')
    search_fields = ('email', 'full_name', 'short_name')


admin.site.register(models.Summoning, SummoningAdmin)
