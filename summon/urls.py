from django.conf.urls import url
from django.contrib import admin

from . import views


admin.autodiscover()


urlpatterns = [
        url(r'^accept/(?P<key>\w+)/?$', views.AcceptView.as_view(),
            name='accept'),

]
