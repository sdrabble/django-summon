import unittest

from django.contrib.sites import models as sites
from django.core import urlresolvers as urls
from django.test.client import RequestFactory
from django.test.utils import override_settings

from summon import backend
from summon import models

from .test_models import TestBase


class TestUrlGeneration(TestBase):

    def setUp(self):
        super(TestUrlGeneration, self).setUp()
        self.summoning = models.Summoning(email=self.email)
        self.summoning.save()
        self.factory = RequestFactory()
        self.site = sites.Site(domain='thesummoning.com', name='Test Site')
        self.site.save()


    def test_get_absolute_url_with_explicit_site(self):
        url = urls.reverse('summon:accept',
                           kwargs={'key': self.summoning.key})
        uri = 'http://thesummoning.com%s' % (url, )
        result = backend.get_absolute_uri(self.summoning, site=self.site)
        self.assertEqual(uri, result)

    @override_settings(SUMMON_SITE_HOST='thesummoning.org:8080')
    def test_get_absolute_url_with_host_setting(self):
        url = urls.reverse('summon:accept',
                           kwargs={'key': self.summoning.key})
        uri = 'http://thesummoning.org:8080%s' % (url, )
        result = backend.get_absolute_uri(self.summoning)
        self.assertEqual(uri, result)

    def test_get_absolute_url_with_request(self):
        url = urls.reverse('summon:accept',
                           kwargs={'key': self.summoning.key})
        uri = 'https://anotherdomain.org%s' % (url, )
        request = self.factory.get(uri)
        # XXX(simon) (Aug 12, 2016): The requested URI gets turned into
        # something that points back to the handling server, not the
        # domain actually requested. This is ok, but it's a little odd
        # that it uses testserver, not example.com.
        result = backend.get_absolute_uri(self.summoning, request=request)
        uri = 'http://testserver%s' % (url, )
        self.assertEqual(uri, result)

    @override_settings(SUMMON_SITE_HOST='thesummoning.org:8080')
    def test_get_absolute_url_with_settings_and_request(self):
        url = urls.reverse('summon:accept',
                           kwargs={'key': self.summoning.key})
        uri = 'https://anotherdomain.org%s' % (url, )
        request = self.factory.get(uri)
        result = backend.get_absolute_uri(self.summoning, request=request)
        uri = 'http://thesummoning.org:8080%s' % (url, )
        self.assertEqual(uri, result)

    @override_settings(SUMMON_SITE_HOST='thesummoning.org:8080')
    def test_get_absolute_url_with_settings_and_site(self):
        url = urls.reverse('summon:accept',
                           kwargs={'key': self.summoning.key})
        result = backend.get_absolute_uri(self.summoning, site=self.site)
        uri = 'http://thesummoning.org:8080%s' % (url, )
        self.assertEqual(uri, result)

    def test_get_absolute_url_with_request_and_site(self):
        url = urls.reverse('summon:accept',
                           kwargs={'key': self.summoning.key})
        uri = 'https://anotherdomain.org%s' % (url, )
        request = self.factory.get(uri)
        result = backend.get_absolute_uri(self.summoning, request=request,
                                          site=self.site)
        uri = 'http://thesummoning.com%s' % (url, )
        self.assertEqual(uri, result)

    @override_settings(SUMMON_SITE_HOST='thesummoning.org:8080')
    def test_get_absolute_url_with_settings_request_and_site(self):
        url = urls.reverse('summon:accept',
                           kwargs={'key': self.summoning.key})
        uri = 'https://anotherdomain.org%s' % (url, )
        request = self.factory.get(uri)
        result = backend.get_absolute_uri(self.summoning, request=request,
                                          site=self.site)
        uri = 'http://thesummoning.org:8080%s' % (url, )
        self.assertEqual(uri, result)

    @override_settings(SITE_URL='https://summonation.org:9876')
    def test_get_absolute_url_with_explicit_site_url(self):
        url = urls.reverse('summon:accept',
                           kwargs={'key': self.summoning.key})
        uri = 'https://anotherdomain.org%s' % (url, )
        request = self.factory.get(uri)
        result = backend.get_absolute_uri(self.summoning, request=request,
                                          site=self.site)
        uri = 'https://summonation.org:9876%s' % (url, )
        self.assertEqual(uri, result)
