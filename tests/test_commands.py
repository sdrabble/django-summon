import datetime
import pytz
import sys
if sys.version_info.major < 3:
    from StringIO import StringIO

else:
    from io import StringIO

import unittest

from django.contrib.auth import get_user_model
from django.core import mail
from django.core.management import call_command
from django.core.management.base import CommandError
from django.test.utils import override_settings
from django.utils import timezone

from summon import backend
from summon import models

from .test_models import TestBase


class TestSummonCommand(TestBase):

    command = 'summon'

    def setUp(self):
        super(TestSummonCommand, self).setUp()
        self.assertEqual([], mail.outbox)
        self.assertEqual(0, models.Summoning.objects.count())

    def test_missing_recipients(self):
        with self.assertRaises(CommandError):
            call_command(self.command)
        self.assertEqual(0, models.Summoning.objects.count())

    def test_valid_summoning(self):
        call_command(self.command, to=['a_recipient@someserver.tld'])
        self.assertEqual(1, models.Summoning.objects.count())
        self.assertEqual(1, len(mail.outbox))

    def test_valid_summoning_with_names(self):
        call_command(self.command,
                     to=['a_recipient@someserver.tld'],
                     full_name='Bob Smedley',
                     short_name='Bob')
        self.assertEqual(1, models.Summoning.objects.count())
        self.assertEqual(1, len(mail.outbox))
        summoning = models.Summoning.objects.all()[0]
        self.assertEqual('Bob Smedley', summoning.full_name)
        self.assertEqual('Bob', summoning.short_name)

    def test_invalid_duplicate_summoning(self):
        email = 'a_recipient@someserver.tld'
        call_command(self.command, to=[email])
        self.assertEqual(1, models.Summoning.objects.count())
        self.assertEqual(1, len(mail.outbox))

        error = StringIO()
        call_command(self.command, to=[email], stderr=error)
        error.seek(0)
        msg = ('Cannot summon "%s" - email will not be sent'
               ' (UNIQUE constraint failed: summon_summoning.email).\n' %
               (email, ))
        self.assertEqual(msg, error.read())
        self.assertEqual(1, models.Summoning.objects.count())
        self.assertEqual(1, len(mail.outbox))

    def test_multiple_recipients(self):
        call_command(self.command, to=['a_recipient@someserver.tld',
                                       'another_guy@someserver.tld'])
        self.assertEqual(2, models.Summoning.objects.count())
        self.assertEqual(2, len(mail.outbox))

    def test_multiple_recipients_with_full_name_is_rejected(self):
        with self.assertRaises(CommandError):
            call_command(self.command,
                         to=['a_recipient@someserver.tld',
                             'another_guy@someserver.tld'],
                         full_name='Bob Smethers'
                         )
        self.assertEqual(0, models.Summoning.objects.count())
        self.assertEqual(0, len(mail.outbox))

    def test_multiple_recipients_with_short_name_is_rejected(self):
        with self.assertRaises(CommandError):
            call_command(self.command,
                         to=['a_recipient@someserver.tld',
                             'another_guy@someserver.tld'],
                         short_name='Bob Smethers'
                         )
        self.assertEqual(0, models.Summoning.objects.count())
        self.assertEqual(0, len(mail.outbox))

    def test_invalid_owner_pk(self):
        with self.assertRaises(get_user_model().DoesNotExist):
            call_command(self.command, to=['a_recipient@someserver.tld'],
                         owner=42)
        self.assertEqual(0, models.Summoning.objects.count())
        self.assertEqual(0, len(mail.outbox))

    def test_invalid_owner_username(self):
        with self.assertRaises(get_user_model().DoesNotExist):
            call_command(self.command, to=['a_recipient@someserver.tld'],
                         owner='an_admin')
        self.assertEqual(0, models.Summoning.objects.count())
        self.assertEqual(0, len(mail.outbox))

    def test_valid_owner_pk(self):
        owner = get_user_model()(username='an_admin')
        owner.save()

        call_command(self.command, to=['a_recipient@someserver.tld'],
                     owner=owner.pk)
        self.assertEqual(1, models.Summoning.objects.count())
        self.assertEqual(1, len(mail.outbox))

    def test_valid_owner_username(self):
        owner = get_user_model()(username='an_admin')
        owner.save()

        call_command(self.command, to=['a_recipient@someserver.tld'],
                     owner=owner.username)
        self.assertEqual(1, models.Summoning.objects.count())
        self.assertEqual(1, len(mail.outbox))

    @override_settings(SUMMON_SEND_ON_SAVE=True)
    @unittest.skip("I don't think this does what it says it does")
    def test_already_sent_does_nothing(self):
        call_command(self.command, to=['a_recipient@someserver.tld'])
        self.assertEqual(1, models.Summoning.objects.count())
        self.assertEqual(1, len(mail.outbox))

    @override_settings(SUMMON_SEND_ON_SAVE=False)
    def test_creation_only(self):
        call_command(self.command, to=['a_recipient@someserver.tld'],
                     create_only=True)
        self.assertEqual(1, models.Summoning.objects.count())
        self.assertEqual(0, len(mail.outbox))

    @override_settings(SUMMON_SEND_ON_SAVE=True)
    def test_creation_only_overridden_by_setting(self):
        call_command(self.command, to=['a_recipient@someserver.tld'],
                     create_only=True)
        self.assertEqual(1, models.Summoning.objects.count())
        self.assertEqual(1, len(mail.outbox))


class TestSummonWithExpirationCommand(TestBase):

    command = 'summon'

    def setUp(self):
        super(TestSummonWithExpirationCommand, self).setUp()
        self.assertEqual([], mail.outbox)
        self.assertEqual(0, models.Summoning.objects.count())
        self.email = 'a_recipient@someserver.tld'

    def test_explicit_expiration_simple_date(self):
        now = datetime.datetime.utcnow()
        expiry = (now + datetime.timedelta(days=30)).date()
        call_command(self.command, to=[self.email],
                     expire=expiry.strftime('%Y-%m-%d'))
        summoning = models.Summoning.objects.get(email=self.email)
        self.assertEqual(expiry, summoning.expiry_date.date())

    def test_explicit_expiration_simple_date_assumes_midnight_local(self):
        now = datetime.datetime.utcnow()
        expiry = (now + datetime.timedelta(days=30)).date()
        call_command(self.command, to=[self.email],
                     expire=expiry.strftime('%Y-%m-%d'))
        summoning = models.Summoning.objects.get(email=self.email)
        new_time = timezone.make_aware(
                datetime.datetime(now.year, now.month, now.day, 0, 0, 0, 0))
        new_time = new_time.astimezone(pytz.utc)
        self.assertEqual(new_time.time(), summoning.expiry_date.time())

    def test_explicit_expiration_naive_datetime_is_converted(self):
        now = datetime.datetime.utcnow()
        expiry = (now + datetime.timedelta(days=30)).replace(microsecond=0)
        call_command(self.command, to=[self.email],
                     expire=expiry.strftime('%Y-%m-%d %H:%M:%S'))
        summoning = models.Summoning.objects.get(email=self.email)
        expiry_utc = timezone.make_aware(expiry).astimezone(pytz.utc)
        self.assertEqual(expiry_utc, summoning.expiry_date)

    def test_explicit_naive_expiration_datetime(self):
        now = datetime.datetime.utcnow()
        expiry = (now + datetime.timedelta(days=30)).replace(microsecond=0)
        call_command(self.command, to=[self.email],
                     expire=expiry.strftime('%Y-%m-%d %H:%M:%S'))
        summoning = models.Summoning.objects.get(email=self.email)
        expiry_utc = timezone.make_aware(expiry).astimezone(pytz.utc)
        self.assertEqual(expiry_utc, summoning.expiry_date)

    def test_explicit_expiration_explicit_timezone(self):
        now = datetime.datetime.utcnow()
        expiry = (now + datetime.timedelta(days=30)).replace(microsecond=0)
        call_command(self.command, to=[self.email],
                     expire=expiry.strftime('%Y-%m-%d %H:%M:%S -0600'))
        summoning = models.Summoning.objects.get(email=self.email)
        tz = pytz.timezone('US/Mountain')
        expiry_mtn = tz.normalize(tz.localize(expiry, is_dst=None))
        self.assertEqual(expiry_mtn, summoning.expiry_date)

    def test_explicit_expiration_explicit_named_timezone(self):
        now = datetime.datetime.utcnow()
        expiry = (now + datetime.timedelta(days=30)).replace(microsecond=0)
        call_command(self.command, to=[self.email],
                     expire=expiry.strftime('%Y-%m-%d %H:%M:%S PDT'))
        summoning = models.Summoning.objects.get(email=self.email)
        tz = pytz.timezone('America/Los_Angeles')
        expiry_mtn = tz.normalize(tz.localize(expiry, is_dst=None))
        self.assertEqual(expiry_mtn, summoning.expiry_date)

    def test_number_of_days(self):
        now = datetime.datetime.utcnow()
        expiry = (now + datetime.timedelta(days=3)).replace(microsecond=0)
        expiry = timezone.make_aware(expiry).astimezone(pytz.utc)

        call_command(self.command, to=[self.email], expire='in 3 days')
        summoning = models.Summoning.objects.get(email=self.email)
        self.assertEqual(expiry.ctime(), summoning.expiry_date.ctime())

    def test_number_of_hours(self):
        now = datetime.datetime.utcnow()
        expiry = (now + datetime.timedelta(hours=6)).replace(microsecond=0)
        expiry = timezone.make_aware(expiry).astimezone(pytz.utc)

        call_command(self.command, to=[self.email], expire='in 6 hours')
        summoning = models.Summoning.objects.get(email=self.email)
        self.assertEqual(expiry.ctime(), summoning.expiry_date.ctime())


@override_settings(SUMMON_SEND_ON_SAVE=True)
class TestResendCommand(TestBase):

    command = 'resend'

    def setUp(self):
        super(TestResendCommand, self).setUp()
        self.assertEqual([], mail.outbox)
        self.assertEqual(0, models.Summoning.objects.count())

    def test_missing_recipients(self):
        with self.assertRaises(CommandError):
            call_command(self.command)
        self.assertEqual(0, models.Summoning.objects.count())

    def test_resend_invalid_accepted_with_message(self):
        email = 'a_recipient@someserver.tld'
        summoning = models.Summoning(email=email)
        summoning.accepted_date = timezone.now()
        summoning.save()

        self.assertEqual(1, models.Summoning.objects.count())
        self.assertEqual(1, len(mail.outbox))

        error = StringIO()
        call_command(self.command, to=[email], verbosity=1, stderr=error)
        error.seek(0)
        msg = 'Cannot re-summon "%s" - already accepted.\n' % (email, )
        self.assertEqual(msg, error.read())
        self.assertEqual(1, models.Summoning.objects.count())
        self.assertEqual(1, len(mail.outbox))

    def test_invalid_email_with_message(self):
        email = 'someguy@someserver.tld'
        error = StringIO()
        call_command(self.command, to=[email], stderr=error, verbosity=1)
        error.seek(0)
        msg = 'Invalid value: "%s"\n' % (email, )
        self.assertEqual(msg, error.read())
        self.assertEqual(0, models.Summoning.objects.count())
        self.assertEqual(0, len(mail.outbox))

    def test_invalid_key_with_message(self):
        key = 'abcd1234'
        error = StringIO()
        call_command(self.command, to=[key], stderr=error, verbosity=1)
        error.seek(0)
        msg = 'Invalid value: "%s"\n' % (key, )
        self.assertEqual(msg, error.read())
        self.assertEqual(0, models.Summoning.objects.count())
        self.assertEqual(0, len(mail.outbox))

    def test_valid_summoning_by_email(self):
        email = 'a_recipient@someserver.tld'
        summoning = models.Summoning(email=email)
        summoning.save()

        self.assertEqual(1, models.Summoning.objects.count())
        self.assertEqual(1, len(mail.outbox))

        call_command(self.command, to=[email])
        self.assertEqual(1, models.Summoning.objects.count())
        self.assertEqual(2, len(mail.outbox))

    def test_valid_summoning_by_key(self):
        email = 'a_recipient@someserver.tld'
        summoning = models.Summoning(email=email)
        summoning.save()

        self.assertEqual(1, models.Summoning.objects.count())
        self.assertEqual(1, len(mail.outbox))

        call_command(self.command, to=[summoning.key])
        self.assertEqual(1, models.Summoning.objects.count())
        self.assertEqual(2, len(mail.outbox))

    def test_valid_summonings(self):
        email = 'a_recipient@someserver.tld'
        email_summoning = models.Summoning(email=email)
        email_summoning.save()

        email = 'another_guy@someserver.tld'
        key_summoning = models.Summoning(email=email)
        key_summoning.save()

        self.assertEqual(2, models.Summoning.objects.count())
        self.assertEqual(2, len(mail.outbox))

        call_command(self.command, to=[email_summoning.email,
                                       key_summoning.key])
        self.assertEqual(2, models.Summoning.objects.count())
        self.assertEqual(4, len(mail.outbox))

    def test_invalid_email_silently_fails(self):
        error = StringIO()
        call_command(self.command, to=['someguy@someserver.tld'], stderr=error,
                     verbosity=0)
        error.seek(0)
        self.assertEqual('', error.read())
        self.assertEqual(0, models.Summoning.objects.count())
        self.assertEqual(0, len(mail.outbox))

    def test_invalid_key_silently_fails(self):
        error = StringIO()
        call_command(self.command, to=['abcd1234'], stderr=error, verbosity=0)
        error.seek(0)
        self.assertEqual('', error.read())
        self.assertEqual(0, models.Summoning.objects.count())
        self.assertEqual(0, len(mail.outbox))

    def test_already_accepted_silently_fails(self):
        email = 'another_guy@someserver.tld'
        summoning = models.Summoning(email=email)
        summoning.accepted_date = timezone.now()
        summoning.save()

        self.assertEqual(1, models.Summoning.objects.count())
        self.assertEqual(1, len(mail.outbox))

        error = StringIO()
        call_command(self.command, to=[summoning.key], stderr=error,
                     verbosity=0)
        error.seek(0)
        self.assertEqual('', error.read())
        self.assertEqual(1, models.Summoning.objects.count())
        self.assertEqual(1, len(mail.outbox))

    def test_resend_all_affects_only_unaccepted(self):
        count = 6
        for x in range(count):
            email = 'another_guy_%d@someserver.tld' % (x + 1, )
            summoning = models.Summoning(email=email)
            if x % 2 == 0:
                summoning.accepted_date = timezone.now()
            summoning.save()

        self.assertEqual(count, models.Summoning.objects.count())
        self.assertEqual(count, len(mail.outbox))

        call_command(self.command, all=True)
        self.assertEqual(count, models.Summoning.objects.count())
        self.assertEqual(count * 1.5, len(mail.outbox))


class TestExpireCommand(TestBase):

    command = 'expiresummons'

    def setUp(self):
        super(TestExpireCommand, self).setUp()
        self.assertEqual(0,
                         models.Summoning.objects.filter(
                                 expiry_date=None).count())

    def test_missing_recipients(self):
        with self.assertRaises(CommandError):
            call_command(self.command)
        self.assertEqual(0, models.Summoning.objects.count())

    def test_valid_summoning_by_email(self):
        email = 'a_recipient@someserver.tld'
        summoning = models.Summoning(email=email)
        summoning.save()

        self.assertEqual(1,
                         models.Summoning.objects.filter(
                                 expiry_date=None).count())

        call_command(self.command, to=[email])
        self.assertEqual(0,
                         models.Summoning.objects.filter(
                                 expiry_date=None).count())

    def test_valid_summoning_by_key(self):
        email = 'a_recipient@someserver.tld'
        summoning = models.Summoning(email=email)
        summoning.save()

        self.assertEqual(1,
                         models.Summoning.objects.filter(
                                 expiry_date=None).count())

        call_command(self.command, to=[summoning.key])
        self.assertEqual(0,
                         models.Summoning.objects.filter(
                                 expiry_date=None).count())

    def test_valid_summonings(self):
        email = 'a_recipient@someserver.tld'
        email_summoning = models.Summoning(email=email)
        email_summoning.save()

        email = 'another_guy@someserver.tld'
        key_summoning = models.Summoning(email=email)
        key_summoning.save()

        self.assertEqual(2,
                         models.Summoning.objects.filter(
                                 expiry_date=None).count())

        call_command(self.command, to=[email_summoning.email,
                                       key_summoning.key])
        self.assertEqual(0,
                         models.Summoning.objects.filter(
                                 expiry_date=None).count())

    def test_invalid_email_silently_fails(self):
        email = 'a_recipient@someserver.tld'
        summoning = models.Summoning(email=email)
        summoning.save()

        error = StringIO()
        call_command(self.command, to=['someguy@someserver.tld'], stderr=error,
                     verbosity=0)
        error.seek(0)
        self.assertEqual('', error.read())
        self.assertEqual(1,
                         models.Summoning.objects.filter(
                                 expiry_date=None).count())

    def test_invalid_key_silently_fails(self):
        email = 'a_recipient@someserver.tld'
        summoning = models.Summoning(email=email)
        summoning.save()

        error = StringIO()
        call_command(self.command, to=['abcd1234'], stderr=error, verbosity=0)
        error.seek(0)
        self.assertEqual('', error.read())
        self.assertEqual(1,
                         models.Summoning.objects.filter(
                                 expiry_date=None).count())

    def test_already_accepted_silently_fails(self):
        email = 'another_guy@someserver.tld'
        summoning = models.Summoning(email=email)
        summoning.accepted_date = timezone.now()
        summoning.save()

        self.assertEqual(1,
                         models.Summoning.objects.filter(
                                 expiry_date=None).count())

        error = StringIO()
        call_command(self.command, to=[summoning.key], stderr=error,
                     verbosity=0)
        error.seek(0)
        self.assertEqual('', error.read())
        self.assertEqual(1,
                         models.Summoning.objects.filter(
                                 expiry_date=None).count())

    def test_invalid_email_fails_with_message(self):
        email = 'a_recipient@someserver.tld'
        summoning = models.Summoning(email=email)
        summoning.save()

        error = StringIO()
        bogus_email = 'someguy@someserver.tld'
        call_command(self.command, to=[bogus_email], stderr=error, verbosity=1)
        error.seek(0)
        msg = 'Invalid value: "%s"\n' % (bogus_email, )
        self.assertEqual(msg, error.read())
        self.assertEqual(1,
                         models.Summoning.objects.filter(
                                 expiry_date=None).count())

    def test_invalid_key_fails_with_message(self):
        email = 'a_recipient@someserver.tld'
        summoning = models.Summoning(email=email)
        summoning.save()

        error = StringIO()
        bogus_key = 'abcd1234'
        call_command(self.command, to=[bogus_key], stderr=error, verbosity=1)
        error.seek(0)
        msg = 'Invalid value: "%s"\n' % (bogus_key, )
        self.assertEqual(msg, error.read())
        self.assertEqual(1,
                         models.Summoning.objects.filter(
                                 expiry_date=None).count())

    def test_already_accepted_fails_with_message(self):
        email = 'another_guy@someserver.tld'
        summoning = models.Summoning(email=email)
        summoning.accepted_date = timezone.now()
        summoning.save()

        self.assertEqual(1,
                         models.Summoning.objects.filter(
                                 expiry_date=None).count())

        error = StringIO()
        call_command(self.command, to=[summoning.key], stderr=error)
        error.seek(0)
        msg = 'Cannot expire "%s" - already accepted.\n' % (summoning.email, )
        self.assertEqual(msg, error.read())
        self.assertEqual(1,
                         models.Summoning.objects.filter(
                                 expiry_date=None).count())

    def test_expire_all_affects_only_unaccepted(self):
        count = 6
        for x in range(count):
            email = 'another_guy_%d@someserver.tld' % (x + 1, )
            summoning = models.Summoning(email=email)
            if x % 2 == 0:
                summoning.accepted_date = timezone.now()
            summoning.save()

        self.assertEqual(count,
                         models.Summoning.objects.filter(
                                 expiry_date=None).count())

        call_command(self.command, all=True)
        self.assertEqual(count / 2,
                         models.Summoning.objects.filter(
                                 expiry_date=None).count())


class TestPurgeCommand(TestBase):

    command = 'purgesummons'

    def setUp(self):
        super(TestPurgeCommand, self).setUp()
        self.assertEqual(0,
                         models.Summoning.objects.filter(
                                 expiry_date=None).count())

        self.assertEqual(0,
                         models.Summoning.objects.filter(
                                 accepted_date=None).count())

    def test_missing_recipients(self):
        with self.assertRaises(CommandError):
            call_command(self.command)
        self.assertEqual(0, models.Summoning.objects.count())

    def test_valid_expired_summoning_by_email(self):
        email = 'a_recipient@someserver.tld'
        summoning = models.Summoning(email=email)
        summoning.expiry_date = timezone.now()
        summoning.save()

        self.assertEqual(1, models.Summoning.objects.count())

        call_command(self.command, to=[email])
        self.assertEqual(0, models.Summoning.objects.count())

    def test_valid_expired_summoning_by_key(self):
        email = 'a_recipient@someserver.tld'
        summoning = models.Summoning(email=email)
        summoning.expiry_date = timezone.now()
        summoning.save()

        self.assertEqual(1, models.Summoning.objects.count())

        call_command(self.command, to=[summoning.key])
        self.assertEqual(0, models.Summoning.objects.count())

    def test_valid_expired_summonings(self):
        email = 'a_recipient@someserver.tld'
        email_summoning = models.Summoning(email=email)
        email_summoning.expiry_date = timezone.now()
        email_summoning.save()

        email = 'another_guy@someserver.tld'
        key_summoning = models.Summoning(email=email)
        key_summoning.expiry_date = timezone.now()
        key_summoning.save()

        email = 'someoneelse@someserver.tld'
        another_summoning = models.Summoning(email=email)
        another_summoning.save()

        self.assertEqual(3, models.Summoning.objects.count())

        call_command(self.command, to=[email_summoning.email,
                                       key_summoning.key])
        self.assertEqual(1, models.Summoning.objects.count())
        self.assertEqual(email, models.Summoning.objects.all()[0].email)

    def test_valid_accepted_summoning_by_email(self):
        email = 'a_recipient@someserver.tld'
        summoning = models.Summoning(email=email)
        summoning.accepted_date = timezone.now()
        summoning.save()

        self.assertEqual(1, models.Summoning.objects.count())

        call_command(self.command, to=[email])
        self.assertEqual(0, models.Summoning.objects.count())

    def test_valid_accepted_summoning_by_key(self):
        email = 'a_recipient@someserver.tld'
        summoning = models.Summoning(email=email)
        summoning.accepted_date = timezone.now()
        summoning.save()

        self.assertEqual(1, models.Summoning.objects.count())

        call_command(self.command, to=[summoning.key])
        self.assertEqual(0, models.Summoning.objects.count())

    def test_valid_accepted_summonings(self):
        email = 'a_recipient@someserver.tld'
        email_summoning = models.Summoning(email=email)
        email_summoning.accepted_date = timezone.now()
        email_summoning.save()

        email = 'another_guy@someserver.tld'
        key_summoning = models.Summoning(email=email)
        key_summoning.accepted_date = timezone.now()
        key_summoning.save()

        email = 'someoneelse@someserver.tld'
        another_summoning = models.Summoning(email=email)
        another_summoning.accepted_date = timezone.now()
        another_summoning.save()

        self.assertEqual(3, models.Summoning.objects.count())

        call_command(self.command, to=[email_summoning.email,
                                       key_summoning.key])
        self.assertEqual(1, models.Summoning.objects.count())
        self.assertEqual(email, models.Summoning.objects.all()[0].email)

    def test_invalid_email_silently_fails(self):
        email = 'a_recipient@someserver.tld'
        summoning = models.Summoning(email=email)
        summoning.save()

        call_command(self.command, to=['someguy@someserver.tld'])
        self.assertEqual(1, models.Summoning.objects.count())

    def test_invalid_key_silently_fails(self):
        email = 'a_recipient@someserver.tld'
        summoning = models.Summoning(email=email)
        summoning.save()

        call_command(self.command, to=['abcd1234'])
        self.assertEqual(1, models.Summoning.objects.count())

    def test_purge_all_affects_only_expired_or_unaccepted(self):
        count = 12
        for x in range(count):
            email = 'another_guy_%d@someserver.tld' % (x + 1, )
            summoning = models.Summoning(email=email)
            if x % 2 == 0:
                summoning.accepted_date = timezone.now()

            if x % 3 == 0:
                summoning.expiry_date = timezone.now()

            summoning.save()

        self.assertEqual(count, models.Summoning.objects.count())

        call_command(self.command, all=True)
        self.assertEqual(4, models.Summoning.objects.count())

    def test_purge_expired(self):
        count = 6
        for x in range(count):
            email = 'another_guy_%d@someserver.tld' % (x + 1, )
            summoning = models.Summoning(email=email)
            if x % 2 == 0:
                summoning.expiry_date = timezone.now()

            summoning.save()

        self.assertEqual(count, models.Summoning.objects.count())

        call_command(self.command, expired=True)
        self.assertEqual(count / 2, models.Summoning.objects.count())

    def test_purge_accepted(self):
        count = 6
        for x in range(count):
            email = 'another_guy_%d@someserver.tld' % (x + 1, )
            summoning = models.Summoning(email=email)
            if x % 2 == 0:
                summoning.accepted_date = timezone.now()

            summoning.save()

        self.assertEqual(count, models.Summoning.objects.count())

        call_command(self.command, accepted=True)
        self.assertEqual(count / 2, models.Summoning.objects.count())

    def test_purge_accepted_and_expired(self):
        count = 12
        for x in range(count):
            email = 'another_guy_%d@someserver.tld' % (x + 1, )
            summoning = models.Summoning(email=email)
            if x % 2 == 0:
                summoning.accepted_date = timezone.now()

            if x % 3 == 0:
                summoning.expiry_date = timezone.now()

            summoning.save()

        self.assertEqual(count, models.Summoning.objects.count())

        call_command(self.command, accepted=True, expired=True)
        self.assertEqual(4, models.Summoning.objects.count())

    def test_purge_accepted_by_owner(self):
        owner = get_user_model()(username='an_admin')
        owner.save()

        email = 'another_guy@someserver.tld'
        accepted_summoning = models.Summoning(email=email, owner=owner)
        accepted_summoning.accepted_date = timezone.now()
        accepted_summoning.save()

        email = 'someguy@someserver.tld'
        summoning = models.Summoning(email=email, owner=owner)
        summoning.expiry_date = timezone.now()
        summoning.save()

        self.assertEqual(2, models.Summoning.objects.count())

        call_command(self.command, owner=owner.pk, accepted=True)
        self.assertEqual(1, models.Summoning.objects.count())
        self.assertIsNone(models.Summoning.objects.all()[0].accepted_date)

    def test_purge_expired_by_owner(self):
        owner = get_user_model()(username='an_admin')
        owner.save()

        email = 'another_guy@someserver.tld'
        expired_summoning = models.Summoning(email=email, owner=owner)
        expired_summoning.expiry_date = timezone.now()
        expired_summoning.save()

        email = 'someguy@someserver.tld'
        summoning = models.Summoning(email=email, owner=owner)
        summoning.accepted_date = timezone.now()
        summoning.save()

        self.assertEqual(2, models.Summoning.objects.count())

        call_command(self.command, owner=owner.pk, expired=True)
        self.assertEqual(1, models.Summoning.objects.count())
        self.assertIsNone(models.Summoning.objects.all()[0].expiry_date)

    def test_purge_by_owner_with_explicit_values(self):
        owner = get_user_model()(username='an_admin')
        owner.save()

        email = 'randomguy@someserver.tld'
        expired_summoning = models.Summoning(email=email)
        expired_summoning.expiry_date = timezone.now()
        expired_summoning.save()

        email = 'someguy@someserver.tld'
        accepted_summoning = models.Summoning(email=email)
        accepted_summoning.accepted_date = timezone.now()
        accepted_summoning.save()

        email = 'someoneelse@someserver.tld'
        unexpired_summoning = models.Summoning(email=email)
        unexpired_summoning.save()

        email = 'hisbrother@someserver.tld'
        unaccepted_summoning = models.Summoning(email=email)
        unaccepted_summoning.save()

        count = 3
        for x in range(count):
            email = 'another_guy_%d@someserver.tld' % (x + 1, )
            summoning = models.Summoning(email=email, owner=owner)
            summoning.save()

        self.assertEqual(count + 4, models.Summoning.objects.count())

        call_command(self.command, owner=owner.pk,
                     to=[unaccepted_summoning.key,
                         unexpired_summoning.email])
        self.assertEqual(2, models.Summoning.objects.count())
        self.assertIsNone(models.Summoning.objects.all()[0].accepted_date)

    def test_purge_expired_by_owner_with_explicit_values(self):
        owner = get_user_model()(username='an_admin')
        owner.save()

        email = 'randomguy@someserver.tld'
        expired_summoning = models.Summoning(email=email)
        expired_summoning.expiry_date = timezone.now()
        expired_summoning.save()

        email = 'someguy@someserver.tld'
        accepted_summoning = models.Summoning(email=email)
        accepted_summoning.accepted_date = timezone.now()
        accepted_summoning.save()

        email = 'someoneelse@someserver.tld'
        unexpired_summoning = models.Summoning(email=email)
        unexpired_summoning.save()

        email = 'hisbrother@someserver.tld'
        unaccepted_summoning = models.Summoning(email=email)
        unaccepted_summoning.save()

        count = 6
        for x in range(count):
            email = 'another_guy_%d@someserver.tld' % (x + 1, )
            summoning = models.Summoning(email=email, owner=owner)
            if x % 2 == 0:
                summoning.expiry_date = timezone.now()

            summoning.save()

        self.assertEqual(count + 4, models.Summoning.objects.count())

        call_command(self.command, owner=owner.pk, expired=True,
                     to=[unaccepted_summoning.key,
                         unexpired_summoning.email])
        self.assertEqual(5, models.Summoning.objects.count())

    def test_invalid_owner_pk(self):
        email = 'another_guy@someserver.tld'
        summoning = models.Summoning(email=email)
        summoning.save()
        self.assertEqual(1, models.Summoning.objects.count())

        with self.assertRaises(get_user_model().DoesNotExist):
            call_command(self.command, owner=42)
        self.assertEqual(1, models.Summoning.objects.count())

    def test_invalid_owner_username(self):
        email = 'another_guy@someserver.tld'
        summoning = models.Summoning(email=email)
        summoning.save()
        self.assertEqual(1, models.Summoning.objects.count())

        with self.assertRaises(get_user_model().DoesNotExist):
            call_command(self.command, owner='an_admin')
        self.assertEqual(1, models.Summoning.objects.count())

    def test_by_owner_pk(self):
        owner = get_user_model()(username='an_admin')
        owner.save()

        email = 'another_guy@someserver.tld'
        summoning = models.Summoning(email=email, owner=owner)
        summoning.save()

        email = 'someguy@someserver.tld'
        summoning = models.Summoning(email=email)
        summoning.expiry_date = timezone.now()
        summoning.save()
        self.assertEqual(2, models.Summoning.objects.count())

        call_command(self.command, owner=owner.pk)
        self.assertEqual(1, models.Summoning.objects.count())

    def test_by_owner_username(self):
        owner = get_user_model()(username='an_admin')
        owner.save()

        email = 'another_guy@someserver.tld'
        summoning = models.Summoning(email=email, owner=owner)
        summoning.save()

        email = 'someguy@someserver.tld'
        summoning = models.Summoning(email=email)
        summoning.expiry_date = timezone.now()
        summoning.save()
        self.assertEqual(2, models.Summoning.objects.count())

        call_command(self.command, owner=owner.username)
        self.assertEqual(1, models.Summoning.objects.count())


class TestGenerateUriCommand(TestBase):

    command = 'getinviteuri'
    email = 'frodo@the.shire'

    def setUp(self):
        super(TestGenerateUriCommand, self).setUp()

        self.summoning = models.Summoning.objects.create(email=self.email)

    def test_by_key(self):
        expected = backend.get_absolute_uri(self.summoning)
        output = StringIO()
        call_command(self.command, key=self.summoning.key, stdout=output)
        output.seek(0)
        self.assertEqual(expected, output.read().strip())

    def test_by_id(self):
        expected = backend.get_absolute_uri(self.summoning)
        output = StringIO()
        call_command(self.command, key=self.summoning.id, stdout=output)
        output.seek(0)
        self.assertEqual(expected, output.read().strip())

    def test_by_email(self):
        expected = backend.get_absolute_uri(self.summoning)
        output = StringIO()
        call_command(self.command, key=self.summoning.email, stdout=output)
        output.seek(0)
        self.assertEqual(expected, output.read().strip())

    def test_bogus(self):
        output = StringIO()
        with self.assertRaises(models.Summoning.DoesNotExist):
            call_command(self.command, key='1234', stdout=output)
