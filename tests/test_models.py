import unittest

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.sites import models as sites
from django.core import mail
from django.core import urlresolvers as urls
from django.db import IntegrityError
from django.test import TestCase
from django.test.utils import override_settings
from django.utils import formats
from django.utils import timezone

from summon import models


class TestBase(TestCase):

    email = 'someones@in.trouble'

    def setUp(self):
        super(TestBase, self).setUp()
        self.user = get_user_model().objects.create(username='sender')


class TestBasic(TestBase):

    def test_creation_no_sender(self):
        summoning = models.Summoning(email=self.email)
        summoning.save()

        self.assertEqual(1, models.Summoning.objects.count())
        the_summoning = models.Summoning.objects.all()[0]
        self.assertIsNotNone(the_summoning.created_date)
        self.assertAlmostEqual(timezone.now().strftime('%s'),
                               the_summoning.created_date.strftime('%s'))
        self.assertIsNotNone(the_summoning.key)
        self.assertEqual(64, len(the_summoning.key))

    def test_creation_with_sender(self):
        summoning = models.Summoning(email=self.email,
                                     owner=self.user)
        summoning.save()

        self.assertEqual(1, models.Summoning.objects.count())
        the_summoning = models.Summoning.objects.all()[0]
        self.assertEqual(self.user, the_summoning.owner)
        self.assertIsNotNone(the_summoning.key)
        self.assertEqual(64, len(the_summoning.key))

    def test_fail_duplicate_email(self):
        summoning = models.Summoning(email=self.email)
        summoning.save()

        bad_summoning = models.Summoning(email=self.email)
        with self.assertRaises(IntegrityError):
            bad_summoning.save()

    def test_fail_duplicate_email_on_edit(self):
        summoning = models.Summoning(email=self.email)
        summoning.save()

        new_summoning = models.Summoning(email='someones@not.in.trouble')
        new_summoning.save()

        summoning.email = 'someones@not.in.trouble'
        with self.assertRaises(IntegrityError):
            summoning.save()


class TestModel(TestBase):

    def setUp(self):
        super(TestModel, self).setUp()
        self.summoning = models.Summoning(email=self.email)
        self.summoning.save()

    def test_get_absolute_url(self):
        url = urls.reverse('summon:accept',
                           kwargs={'key': self.summoning.key})
        self.assertEqual('http://example.com%s' % (url, ),
                         self.summoning.get_absolute_url())

    def test_is_expired(self):
        self.assertFalse(self.summoning.is_expired)
        self.summoning.expiry_date = timezone.now() - timezone.timedelta(days=1)
        self.summoning.save()
        self.assertTrue(self.summoning.is_expired)

    def test_is_expired_default(self):
        self.assertIsNone(self.summoning.expiry_date)
        self.assertFalse(self.summoning.is_expired)
        self.summoning.expiry_date = timezone.now() - timezone.timedelta(days=1)
        self.summoning.save()
        self.assertTrue(self.summoning.is_expired)

    def test_is_accepted(self):
        self.assertFalse(self.summoning.is_accepted)
        self.summoning.accepted_date = timezone.now()
        self.summoning.save()
        self.assertTrue(self.summoning.is_accepted)


class TestSend(TestBase):

    def setUp(self):
        super(TestSend, self).setUp()
        self.site = sites.Site.objects.get(domain='example.com')
        self.site.name = 'The Best Site'
        self.site.save()

        self.summoning = models.Summoning(email=self.email)
        self.summoning.save()

        self.assertIsNone(self.summoning.sent_date)
        self.assertEqual([], mail.outbox)

    def test_send(self):
        self.summoning.send()
        self.assertIsNotNone(self.summoning.sent_date)

        self.assertEqual(1, len(mail.outbox))
        msg = mail.outbox[0]
        self.assertEqual([self.email], msg.to)
        self.assertEqual('Your invitation to join %s!' % (self.site.name, ),
                         msg.subject)

        link = urls.reverse('summon:accept',
                            kwargs={'key': self.summoning.key})
        self.assertTrue(link in msg.body)
        self.assertTrue(self.summoning.email in msg.body)
        self.assertTrue(self.site.name in msg.body)

    @override_settings(SUMMON_SITE_NAME='The World of Testing')
    def test_send_with_site(self):
        self.summoning.send()
        self.assertIsNotNone(self.summoning.sent_date)

        self.assertEqual(1, len(mail.outbox))
        msg = mail.outbox[0]
        self.assertEqual([self.email], msg.to)
        self.assertEqual('Your invitation to join The World of Testing!',
                         msg.subject)
        link = urls.reverse('summon:accept',
                            kwargs={'key': self.summoning.key})
        self.assertTrue(link in msg.body)
        self.assertTrue('The World of Testing' in msg.body)

    @override_settings(SUMMON_ACCEPT_URL='invitation:accept',
                       ROOT_URLCONF='tests.invitation_urls')
    def test_send_with_named_url(self):
        self.summoning.send()
        self.assertIsNotNone(self.summoning.sent_date)

        self.assertEqual(1, len(mail.outbox))
        msg = mail.outbox[0]
        link = urls.reverse('invitation:accept',
                            kwargs={'key': self.summoning.key})
        self.assertTrue(link in msg.body)

    @override_settings(SUMMON_ACCEPT_URL='bogus-invite',
                       ROOT_URLCONF='tests.invitation_urls')
    def test_send_with_bogus_url_fails(self):
        with self.assertRaises(urls.NoReverseMatch):
            self.summoning.send()
        self.assertIsNone(self.summoning.sent_date)
        self.assertEqual(0, len(mail.outbox))

    @override_settings(SUMMON_FROM_EMAIL='Bob Rickets <bob@somewhere.com>')
    def test_send_with_from(self):
        self.summoning.send()
        self.assertIsNotNone(self.summoning.sent_date)

        self.assertEqual(1, len(mail.outbox))
        msg = mail.outbox[0]
        self.assertEqual('Bob Rickets <bob@somewhere.com>', msg.from_email)

    @override_settings(SUMMON_FROM_EMAIL='Bob Rickets <bob@somewhere.com>',
                       DEFAULT_FROM_EMAIL='Sandy Crevass <sandy@somewhere.com>')
    def test_send_with_from_and_default(self):
        self.summoning.send()
        self.assertIsNotNone(self.summoning.sent_date)

        self.assertEqual(1, len(mail.outbox))
        msg = mail.outbox[0]
        self.assertEqual('Bob Rickets <bob@somewhere.com>', msg.from_email)

    @override_settings(DEFAULT_FROM_EMAIL='Sandy Crevass <sandy@somewhere.com>')
    def test_send_with_from_and_default(self):
        self.summoning.send()
        self.assertIsNotNone(self.summoning.sent_date)

        self.assertEqual(1, len(mail.outbox))
        msg = mail.outbox[0]
        self.assertEqual('Sandy Crevass <sandy@somewhere.com>', msg.from_email)

    def test_send_with_custom_subject_template(self):
        self.summoning.send(subject_template='tests/subject.txt')

        self.assertEqual(1, len(mail.outbox))
        msg = mail.outbox[0]
        self.assertEqual('This is a custom subject.', msg.subject)

    def test_send_with_custom_body_template(self):
        self.summoning.send(body_template='tests/body.txt')

        self.assertEqual(1, len(mail.outbox))
        msg = mail.outbox[0]
        self.assertEqual('Here is the custom body.', msg.body)


@override_settings(SUMMON_EXPIRE_INVITES=True)
class TestSendWithExpiry(TestBase):

    def setUp(self):
        super(TestSendWithExpiry, self).setUp()
        self.summoning = models.Summoning(email=self.email)
        self.summoning.save()

        self.assertIsNone(self.summoning.sent_date)
        self.assertEqual([], mail.outbox)

    def test_send_with_expiry(self):
        self.summoning.send()
        self.assertIsNotNone(self.summoning.sent_date)

        self.assertEqual(1, len(mail.outbox))
        msg = mail.outbox[0]
        self.assertEqual([self.email], msg.to)

        expiry = self.summoning.expiry_date
        expiry = timezone.template_localtime(expiry, use_tz=False)
        formatted_expiry = formats.date_format(expiry,
                                               settings.DATETIME_FORMAT,
                                               use_l10n=False)
        self.assertTrue(formatted_expiry in msg.body)
        self.assertTrue(expiry.tzinfo.tzname(expiry) in msg.body)

    @override_settings(TIME_ZONE='US/Eastern')
    def test_send_with_expiry_with_timezone(self):
        self.summoning.send()
        self.assertIsNotNone(self.summoning.sent_date)

        self.assertEqual(1, len(mail.outbox))
        msg = mail.outbox[0]
        self.assertEqual([self.email], msg.to)

        expiry = self.summoning.expiry_date
        expiry = timezone.template_localtime(expiry, use_tz=False)
        formatted_expiry = formats.date_format(expiry,
                                               settings.DATETIME_FORMAT,
                                               use_l10n=False)
        self.assertTrue(formatted_expiry in msg.body)
        self.assertTrue('UTC' in msg.body)


class TestSendOnSave(TestBase):

    def setUp(self):
        super(TestSendOnSave, self).setUp()
        self.site = sites.Site.objects.get(domain='example.com')
        self.site.name = 'The Best Site'
        self.site.save()

    @override_settings(SUMMON_SEND_ON_SAVE=True)
    def test_send_on_save(self):
        self.assertEqual([], mail.outbox)

        summoning = models.Summoning(email=self.email)
        summoning.save()

        self.assertEqual(1, len(mail.outbox))
        msg = mail.outbox[0]
        self.assertEqual([self.email], msg.to)
        self.assertEqual('Your invitation to join %s!' % (self.site.name, ),
                         msg.subject)

        link = urls.reverse('summon:accept', kwargs={'key': summoning.key})
        self.assertTrue(link in msg.body)
