from django.conf.urls import include, url


from . import views


urlpatterns = [
    url(r'^invite/', include('summon.urls', namespace='invitation')),
    url(r'^bogus-invite/', views.default, name='bogus-invite'),
]
