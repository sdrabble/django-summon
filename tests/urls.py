from django.conf.urls import include, url


from . import views


urlpatterns = [
    url(r'^invite/', include('summon.urls', namespace='summon')),
    url(r'^accounts/signup/(?P<key>\w+)/?$', views.default,
        name='account_signup_with_key'),
    url(r'^accounts/signup/?$', views.default, name='account_signup'),
    url(r'^accounts/login/?$', views.default, name='account_login'),
    url(r'^member/register/?$', views.default, name='member_register'),
    url(r'^member/login/?$', views.default, name='member_login'),
]
