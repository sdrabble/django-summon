import sys
import unittest
if sys.version_info.major < 3:
    from urllib2 import urlparse

else:
    from urllib import parse as urlparse

from django.contrib.sites import models as sites
from django.core import urlresolvers as urls
from django.test.client import RequestFactory
from django.test.utils import override_settings
from django.utils import crypto
from django.utils import timezone

from summon import backend
from summon import models

from .test_models import TestBase


class TestAcceptSummoningBase(TestBase):

    def setUp(self):
        super(TestAcceptSummoningBase, self).setUp()
        self.summoning = models.Summoning(email=self.email)
        self.summoning.save()
        self.factory = RequestFactory()


class AcceptSummoningTest(TestAcceptSummoningBase):

    def test_valid_redirect(self):
        url = urls.reverse('summon:accept', kwargs={'key': self.summoning.key})

        response = self.client.get(url)
        self.assertEqual(302, response.status_code)

        location = response['Location']
        uri = urlparse.urlparse(location)
        url = urls.resolve(uri.path)
        self.assertEqual(':account_signup',
                         ':'.join((url.namespace, url.url_name)))
        summoning = models.Summoning.objects.get(pk=self.summoning.pk)
        self.assertTrue(summoning.accepted_date)

    @override_settings(SUMMON_REGISTER_URL='member_register')
    def test_valid_redirect_with_url_name_setting(self):
        url = urls.reverse('summon:accept', kwargs={'key': self.summoning.key})

        response = self.client.get(url)
        self.assertEqual(302, response.status_code)

        location = response['Location']
        uri = urlparse.urlparse(location)
        url = urls.resolve(uri.path)
        self.assertEqual(':member_register',
                         ':'.join((url.namespace, url.url_name)))

    @override_settings(SUMMON_REGISTER_URL=urls.reverse_lazy('member_register'))
    def test_valid_redirect_with_url_setting(self):
        url = urls.reverse('summon:accept', kwargs={'key': self.summoning.key})

        response = self.client.get(url)
        self.assertEqual(302, response.status_code)

        location = response['Location']
        uri = urlparse.urlparse(location)
        url = urls.resolve(uri.path)
        self.assertEqual(':member_register',
                         ':'.join((url.namespace, url.url_name)))

    @override_settings(SUMMON_REGISTER_URL='http://anothersite.com/register')
    def test_valid_redirect_with_explicit_url(self):
        url = urls.reverse('summon:accept', kwargs={'key': self.summoning.key})

        response = self.client.get(url)
        self.assertEqual(302, response.status_code)

        location = response['Location']
        self.assertEqual('http://anothersite.com/register', location)


class ForwardedArgsTest(TestAcceptSummoningBase):

    @override_settings(SUMMON_FORWARD_ARGS=True,
                       SUMMON_REGISTER_URL='account_signup_with_key')
    def test_valid_redirect_with_params(self):
        url = urls.reverse('summon:accept', kwargs={'key': self.summoning.key})

        response = self.client.get(url)
        self.assertEqual(302, response.status_code)

        location = response['Location']
        uri = urlparse.urlparse(location)
        url = urls.resolve(uri.path)
        self.assertEqual(':account_signup_with_key',
                         ':'.join((url.namespace, url.url_name)))
        self.assertEqual({'key': self.summoning.key}, url.kwargs)

    @override_settings(APPEND_SLASH=False,
                       SUMMON_FORWARD_ARGS=True,
                       SUMMON_REGISTER_URL='/my/signup')
    def test_explicit_redirect_with_forced_forward(self):
        url = urls.reverse('summon:accept', kwargs={'key': self.summoning.key})

        response = self.client.get(url)
        self.assertEqual(302, response.status_code)

        location = response['Location']
        uri = urlparse.urlparse(location)
        self.assertEqual('/my/signup/{0}'.format(self.summoning.key), uri.path)

    @override_settings(SUMMON_APPEND_SLASH=True,
                       SUMMON_FORWARD_ARGS=True,
                       SUMMON_REGISTER_URL='/my/signup/with/slash')
    def test_explicit_redirect_with_forced_forward_and_trailing_slash(self):
        url = urls.reverse('summon:accept', kwargs={'key': self.summoning.key})

        response = self.client.get(url)
        self.assertEqual(302, response.status_code)

        location = response['Location']
        uri = urlparse.urlparse(location)
        self.assertEqual('/my/signup/with/slash/{0}/'.format(self.summoning.key),
                         uri.path)

    @override_settings(APPEND_SLASH=False,
                       SUMMON_FORWARD_ARGS=True,
                       SUMMON_REGISTER_URL='/my/signup/with/slash/')
    def test_explicit_redirect_with_forced_forward_does_not_double_slash(self):
        url = urls.reverse('summon:accept', kwargs={'key': self.summoning.key})

        response = self.client.get(url)
        self.assertEqual(302, response.status_code)

        location = response['Location']
        uri = urlparse.urlparse(location)
        self.assertEqual('/my/signup/with/slash/{0}'.format(self.summoning.key),
                         uri.path)

    @override_settings(SUMMON_FORWARD_ARGS=True,
                       SUMMON_FORWARD_ARGS_VIA_QUERY_STRING=True,
                       SUMMON_REGISTER_URL='/my/signup')
    def test_explicit_redirect_with_forced_forward_via_query_string(self):
        url = urls.reverse('summon:accept', kwargs={'key': self.summoning.key})

        response = self.client.get(url)
        self.assertEqual(302, response.status_code)

        location = response['Location']
        uri = urlparse.urlparse(location)
        self.assertEqual('/my/signup'.format(self.summoning.key), uri.path)
        self.assertEqual('key={0}'.format(self.summoning.key), uri.query)

    @override_settings(SUMMON_REGISTER_URL='http://anothersite.com/register',
                       SUMMON_FORWARD_ARGS=True)
    def test_redirect_with_new_netloc(self):
        url = urls.reverse('summon:accept', kwargs={'key': self.summoning.key})

        response = self.client.get(url)
        self.assertEqual(302, response.status_code)

        location = response['Location']
        url = 'http://anothersite.com/register/{}/'.format(self.summoning.key)
        self.assertEqual(url, location)


class AcceptOnLoadTest(TestAcceptSummoningBase):

    @override_settings(SUMMON_ACCEPT_ON_LOAD=True)
    def test_accept_on_load_redirects_to_login_on_reload(self):
        accept_url = urls.reverse('summon:accept',
                                  kwargs={'key': self.summoning.key})

        response = self.client.get(accept_url)
        self.assertEqual(302, response.status_code)
        location = response['Location']
        uri = urlparse.urlparse(location)
        url = urls.resolve(uri.path)
        self.assertEqual(':account_signup',
                         ':'.join((url.namespace, url.url_name)))

        response = self.client.get(accept_url)
        self.assertEqual(302, response.status_code)
        location = response['Location']
        uri = urlparse.urlparse(location)
        url = urls.resolve(uri.path)
        self.assertEqual(':account_login',
                         ':'.join((url.namespace, url.url_name)))

    @override_settings(SUMMON_ACCEPT_ON_LOAD=False)
    def test_accept_on_registration_does_not_redirect_to_login_on_reload(self):
        accept_url = urls.reverse('summon:accept',
                                  kwargs={'key': self.summoning.key})

        response = self.client.get(accept_url)
        self.assertEqual(302, response.status_code)

        location = response['Location']
        uri = urlparse.urlparse(location)
        url = urls.resolve(uri.path)
        self.assertEqual(':account_signup',
                         ':'.join((url.namespace, url.url_name)))

        response = self.client.get(accept_url)
        self.assertEqual(302, response.status_code)

        location = response['Location']
        uri = urlparse.urlparse(location)
        url = urls.resolve(uri.path)
        self.assertEqual(':account_signup',
                         ':'.join((url.namespace, url.url_name)))

    @override_settings(SUMMON_ACCEPT_ON_LOAD=False)
    def test_accept_on_registration_does_not_accept_on_load(self):
        url = urls.reverse('summon:accept', kwargs={'key': self.summoning.key})

        self.client.get(url)
        summon = models.Summoning.objects.get(pk=self.summoning.pk)
        self.assertIsNone(summon.accepted_date)


class InvalidSummoningTest(TestAcceptSummoningBase):

    def test_invalid_key(self):
        random_key = crypto.get_random_string(64)
        url = urls.reverse('summon:accept', kwargs={'key': random_key})

        response = self.client.get(url)
        self.assertEqual(404, response.status_code)
        self.assertTemplateUsed(response, 'summon/error/invalid_key.html')

    def test_expired_key(self):
        self.summoning.expiry_date = timezone.now() - timezone.timedelta(days=1)
        self.summoning.save()
        url = urls.reverse('summon:accept',
                           kwargs={'key': self.summoning.key})

        response = self.client.get(url)
        self.assertEqual(410, response.status_code)
        self.assertTemplateUsed(response, 'summon/error/expired_key.html')

    def test_accepted_key(self):
        self.summoning.accept()
        url = urls.reverse('summon:accept',
                           kwargs={'key': self.summoning.key})

        response = self.client.get(url)
        self.assertEqual(302, response.status_code)
        location = response['Location']
        uri = urlparse.urlparse(location)
        url = urls.resolve(uri.path)
        self.assertEqual(':account_login',
                         ':'.join((url.namespace, url.url_name)))

    @override_settings(SUMMON_LOGIN_URL=urls.reverse_lazy('member_login'))
    def test_accepted_key_with_resolved_login_url_setting(self):
        self.summoning.accept()
        url = urls.reverse('summon:accept',
                           kwargs={'key': self.summoning.key})

        response = self.client.get(url)
        self.assertEqual(302, response.status_code)
        location = response['Location']
        uri = urlparse.urlparse(location)
        url = urls.resolve(uri.path)
        self.assertEqual(':member_login',
                         ':'.join((url.namespace, url.url_name)))

    @override_settings(SUMMON_LOGIN_URL='member_login')
    def test_accepted_key_with_login_name_setting(self):
        self.summoning.accept()
        url = urls.reverse('summon:accept',
                           kwargs={'key': self.summoning.key})

        response = self.client.get(url)
        self.assertEqual(302, response.status_code)
        location = response['Location']
        uri = urlparse.urlparse(location)
        url = urls.resolve(uri.path)
        self.assertEqual(':member_login',
                         ':'.join((url.namespace, url.url_name)))

    @override_settings(SUMMON_LOGIN_URL='/member/login/')
    def test_accepted_key_with_login_path_setting(self):
        self.summoning.accept()
        url = urls.reverse('summon:accept',
                           kwargs={'key': self.summoning.key})

        response = self.client.get(url)
        self.assertEqual(302, response.status_code)
        location = response['Location']
        uri = urlparse.urlparse(location)
        url = urls.resolve(uri.path)
        self.assertEqual(':member_login',
                         ':'.join((url.namespace, url.url_name)))

    @override_settings(LOGIN_URL='/member/login/')
    def test_accepted_key_with_standard_login_url_setting(self):
        self.summoning.accept()
        url = urls.reverse('summon:accept',
                           kwargs={'key': self.summoning.key})

        response = self.client.get(url)
        self.assertEqual(302, response.status_code)
        location = response['Location']
        uri = urlparse.urlparse(location)
        url = urls.resolve(uri.path)
        self.assertEqual(':member_login',
                         ':'.join((url.namespace, url.url_name)))
