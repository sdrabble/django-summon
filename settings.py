import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


SECRET_KEY = 'fake-key'
INSTALLED_APPS = [
        'django.contrib.auth',
        'django.contrib.sessions',
        'django.contrib.sites',
        'django.contrib.messages',
        'django.contrib.contenttypes',
        'django.contrib.admin',
        'summon',
        'tests',
]


try:
    import captcha
    INSTALLED_APPS += ['captcha']

except ImportError as e:
    pass


MIDDLEWARE_CLASSES = [
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware'
]


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'sqlite3.db'),
    },
}


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },

        'console': {
             'level': 'DEBUG',
             'class': 'logging.StreamHandler',
             'formatter': 'verbose'
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins', 'console'],
            'level': 'DEBUG',
            'propagate': True,
        },

        'django_summon': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },

    }
}

TIME_ZONE = 'MST'
USE_I18N = True
USE_L10N = True
USE_TZ = True
DEBUG = True
SITE_ID = 1

EMAIL_BACKEND = 'django.core.mail.backends.locmem.EmailBackend'

ROOT_URLCONF = 'urls'
