#!/usr/bin/env python
import os
import sys

import django
from django.conf import settings
from django.test.utils import get_runner


if __name__ == '__main__':
    os.environ['DJANGO_SETTINGS_MODULE'] = 'tests.settings_test'
    django.setup()
    TestRunner = get_runner(settings)
    test_runner = TestRunner()
    if len(sys.argv) > 1:
        tests_to_run = sys.argv[1]

    else:
        tests_to_run = 'tests'

    failures = test_runner.run_tests([tests_to_run])
    sys.exit(bool(failures))
