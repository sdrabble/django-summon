#!/usr/bin/env python
import os
import sys
from setuptools import find_packages, setup
from summon import VERSION


with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
        README = readme.read()


# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='django-summon',
    version='.'.join(map(str, VERSION)),
    packages=find_packages(exclude=["tests.*", "tests"]),
    include_package_data=True,
    license='BSD License',
    description='A Django app to allow inviting users.',
    long_description=README,
    url='',
    author='Simon Drabble',
    author_email='simon.drabble@gmail.com',
    install_requires=[
        'dateparser',
    ],
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)
